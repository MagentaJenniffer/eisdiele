FROM openjdk:8u181-jre-slim

COPY app.jar /

ENV PORT 8080
EXPOSE $PORT
CMD [ "sh", "-c", "java -jar -Dserver.port=${PORT} app.jar" ]